#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Recommender engine
 * @G: graph represented as adjacency matrix. The matrix is a *flat* 2D-array,
 *     is accessed via single index: G[i][j] => G[i * @V + j].
 * @V: number of nodes in graph.
 * @R: recommendation for each user, as 1D-array of @V entries.
 *
 * This function is not be to implemented here, but in one of the sg_* files.
 */
void sg_recommender(unsigned long *G, size_t V, unsigned long *R);

unsigned long *read_graph(FILE *fi, size_t *_V)
{
	size_t V, E;
	unsigned long *G;

	if (fscanf(fi, "%zu\n%zu\n", &V, &E) != 2)
		goto err;

	/* Check that V is a non-0 power of two */
	if (!V || (V & (V - 1)))
		goto err;

	/* Align allocation a cache line (64 bytes) */
	G = aligned_alloc(64, V * V * sizeof(unsigned long));
	memset(G, 0, V * V * sizeof(unsigned long));

	/* Populate edges */
	while (E--) {
		size_t i, j;
		unsigned long l;

		if (fscanf(fi, "%zu %zu %lu\n", &i, &j, &l) != 3)
			goto err;

		G[i * V + j] = l;
	}

	*_V = V;
	return G;

err:
	fprintf(stderr, "Incorrect input file format\n");
	exit(1);
}

void write_rec(FILE *fo, unsigned long *R, size_t V)
{
	size_t i;

	for (i = 0; i < V; i++)
		fprintf(fo, "%zu %zu\n", i, R[i]);
}

int main(int argc, char *argv[])
{
	FILE *fi, *fo;
	size_t V;
	unsigned long *G, *R;

	if (argc != 3) {
		fprintf(stderr, "Usage: %s infile outfile\n", argv[0]);
		exit(1);
	}

	fi = fopen(argv[1], "r");
	fo = fopen(argv[2], "w");
	if (!fi || !fo) {
		fprintf(stderr, "Could not open input or output file\n");
		exit(1);
	}

	G = read_graph(fi, &V);

	/* Align allocation a cache line (64 bytes) */
	R = aligned_alloc(64, V * sizeof(unsigned long));

	/* !!! Calling your implementation !!! */
	sg_recommender(G, V, R);

	write_rec(fo, R, V);

	fclose(fi);
	fclose(fo);

	free(G);
	free(R);

	return 0;
}
