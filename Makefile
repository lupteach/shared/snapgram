# Three versions of snapgram
BIN := snapgram_serial snapgram_thread snapgram_block

CFLAGS := -Wall -Wextra -Werror -O2

all: $(BIN)

snapgram_%: snapgram.c sg_%.c
	$(CC) $(CFLAGS) -o $@ $^

datasets: create_dataset
	for i in 8 512 1024 2048; \
		do ./create_dataset $$i 0.9 42 > $$i.txt; \
		done

create_dataset: create_dataset.c
	$(CC) $(CFLAGS) -o $@ $<

clean:
	-rm $(BIN)

distclean:
	-rm *.txt *.ref
