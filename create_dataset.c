#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/*
 * Program helpers
 */
#define die(...)                    \
do {                                \
	fprintf(stderr, ##__VA_ARGS__); \
	fprintf(stderr, "\n");          \
	exit(1);                        \
} while (0)

#define die_perror(s) \
do {                  \
	perror(s);        \
	exit(1);          \
} while (0)

static inline void *xcalloc(size_t nmemb, size_t size)
{
	void *p = calloc(nmemb, size);

	if (!p)
		die_perror("calloc");
	return p;
}

void usage(char *program)
{
	die("Usage: %s V ratio [seed]\n"
		"  V     : number of nodes in graph\n"
		"  ratio : graph connectivity\n"
		"          (e.g. '0.1' to connect each node to about 10%% of the graph)\n"
		"  seed  : seed for random number generator (0 by default)",
		program);
}

/*
 * Graph building
 */

/*
 * 2d array access -- @A: 1D array access as a 2D array, @W: width, @r and @c:
 * row and column of the access
 */
#define MAT(A, W, r, c) (A[(r) * (W) + (c)])

size_t init_graph(unsigned long *G, size_t V, double ratio)
{
	size_t i, j;
	size_t E = 0;

	for (i = 0; i < V; i++) {
		for (j = 0; j < V; j++) {
			/* Cannot match oneself */
			if (i == j)
				continue;

			if (drand48() > ratio)
				continue;

			MAT(G, V, i, j) = drand48() * UINT8_MAX + 1;
			E++;
		}
	}

	return E;
}

void print_graph(unsigned long *G, size_t V, size_t E)
{
	size_t i, j;

	printf("%zu\n", V);
	printf("%zu\n", E);

	for (i = 0; i < V; i++) {
		for (j = 0; j < V; j++) {
			if (MAT(G, V, i, j) > 0)
				printf("%zu %zu %lu\n", i, j, MAT(G, V, i, j));
		}
	}
}

int main(int argc, char *argv[])
{
	long seed;
	double ratio;
	unsigned long *G;
	size_t V, E;

	/* Command line arguments */
	if (argc < 3)
		usage (argv[0]);

	V = atol(argv[1]);
	if (V > UINT16_MAX)
		die ("Error: wrong number of nodes (V <= %d)", UINT16_MAX);

	ratio = atof(argv[2]);
	if (ratio <= 0.0 || ratio > 1.0)
		die ("Error: wrong connectivity ratio (0.0 < ratio < 1.0)");

	if (argc > 3) {
		seed = atol(argv[3]);
	} else {
		seed = 0;
	}
	srand48(seed);

	/* Build graph as adjacency matrix */
	G = xcalloc(V * V, sizeof(unsigned long));
	E = init_graph(G, V, ratio);

	/* Output graph */
	print_graph(G, V, E);
}
