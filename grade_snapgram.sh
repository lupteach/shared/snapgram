#!/bin/bash

# Safer execution
# -e: exit immediately if a command fails
# -E: Safer -e option for traps
# -u: fail if a variable is used unset
# -o pipefail: exit immediately if command in a pipe fails
#set -eEuo pipefail
# -x: print each command before executing (great for debugging)
#set -x

# Convenient values
SCRIPT_NAME=$(basename $BASH_SOURCE)

# Default program values
TEST_CASE="all"

#
# Logging helpers
#
log() {
	echo -e "${*}"
}
info() {
	log "Info: ${*}"
}
warning() {
	log "Warning: ${*}"
}
error() {
	log "Error: ${*}"
}
die() {
	error "${*}"
	exit 1
}

#
# Line comparison
#
select_line() {
	# 1: string
	# 2: line to select
	echo "$(echo "${1}" | sed "${2}q;d")"
}

fail() {
	# 1: got
	# 2: expected
	log "Fail: got '${1}' but expected '${2}'"
}

pass() {
	# got
	log "Pass: ${1}"
}

compare_lines() {
	# 1: output
	# 2: expected
	# 3: score (output)
	declare -a output_lines=("${!1}")
	declare -a expect_lines=("${!2}")
	local __score=$3
	local partial="0"

	# Amount of partial credit for each correct output line
	local step=$(bc -l <<< "1.0 / ${#expect_lines[@]}")

	# Compare lines, two by two
	for i in ${!output_lines[*]}; do
		if [[ "${output_lines[${i}]}" =~ "${expect_lines[${i}]}" ]]; then
			pass "${output_lines[${i}]}"
			partial=$(bc <<< "${partial} + ${step}")
		else
			fail "${output_lines[${i}]}" "${expect_lines[${i}]}" ]]
		fi
	done

	# Return final score
	eval ${__score}="'${partial}'"
}

#
# Run generic test case
#
run_test_case() {
	local exec="${1}"

	[[ -x "$(command -v ${exec})" ]] || \
		die "Cannot find executable '${exec}'"

	# These are global variables after the test has run so clear them out now
	unset STDOUT STDERR RET

	# Create temp files for getting stdout and stderr
	local outfile=$(mktemp)
	local errfile=$(mktemp)

	"${@}" >${outfile} 2>${errfile}

	# Get the return status, stdout and stderr of the test case
	RET="${?}"
	STDOUT=$(cat "${outfile}")
	STDERR=$(cat "${errfile}")

	# Clean up temp files
	rm -f "${outfile}"
	rm -f "${errfile}"
}

run_time() {
	#1: num repetitions
	local reps="${1}"
	shift
	local exec="${1}"

	[[ -x ${exec} ]] || \
		die "Cannot find executable '${exec}'"

	# These are global variables after the test has run so clear them out now
	unset PERF

	for i in $(seq ${reps}); do
		# Create temp files for getting stdout and stderr
		local outfile=$(mktemp)
		local errfile=$(mktemp)

		TIME="%e" /usr/bin/time "${@}" >${outfile} 2>${errfile}

		# Last line of stderr
		local t=$(cat "${errfile}" | tail -n1)

		# Check it's the right format
		if [[ ! "${t}" =~ ^[0-9]{1,3}\.[0-9]{2}$ ]]; then
			die "Wrong timing output '${t}'"
		fi

		# Keep the best timing
		if [ -z "${PERF}" ]; then
			PERF=${t}
		elif (( $(bc <<<"${t} < ${PERF}") )); then
			PERF=${t}
		fi

		# Clean up temp files
		rm -f "${outfile}"
		rm -f "${errfile}"
	done
}

#
# Test cases
#
TEST_CASES=()

#
# Correctness
#
serial_correct() {

	local line_array=()
	local corr_array=()
	for i in 8 512 1024 2048; do
		run_test_case ./snapgram_serial "${i}.txt" "${i}.sout"

		run_test_case diff -s "${i}.ref" "${i}.sout"

		line_array+=("$(select_line "${STDOUT}" "1")")
		corr_array+=("Files ${i}.ref and ${i}.sout are identical")

		rm -f "${i}.sout"
	done

	local score
	compare_lines line_array[@] corr_array[@] score
	log "${score}"
}
TEST_CASES+=("serial_correct")

thread_correct() {

	local line_array=()
	local corr_array=()
	for i in 8 512 1024 2048; do
		run_test_case ./snapgram_thread "${i}.txt" "${i}.tout"

		run_test_case diff -s "${i}.ref" "${i}.tout"

		line_array+=("$(select_line "${STDOUT}" "1")")
		corr_array+=("Files ${i}.ref and ${i}.tout are identical")

		rm -f "${i}.tout"
	done

	local score
	compare_lines line_array[@] corr_array[@] score
	log "${score}"
}
TEST_CASES+=("thread_correct")

block_correct() {

	local line_array=()
	local corr_array=()
	for i in 8 512 1024 2048; do
		run_test_case ./snapgram_block "${i}.txt" "${i}.bout"

		run_test_case diff -s "${i}.ref" "${i}.bout"

		line_array+=("$(select_line "${STDOUT}" "1")")
		corr_array+=("Files ${i}.ref and ${i}.bout are identical")

		rm -f "${i}.bout"
	done

	local score
	compare_lines line_array[@] corr_array[@] score
	log "${score}"
}
TEST_CASES+=("block_correct")

#
# Speed
#
serial_speed()
{
	run_time 2 ./ref_snapgram_serial 1024.txt /dev/null
	local ref_perf=${PERF}

	run_time 2 ./snapgram_serial 1024.txt /dev/null
	local tst_perf=${PERF}

	local ratio=$(bc -l <<<"${tst_perf} / ${ref_perf}")
	log "${ratio}"
}
TEST_CASES+=("serial_speed")

thread_speed()
{
	run_time 2 ./ref_snapgram_thread 2048.txt /dev/null
	local ref_perf=${PERF}

	run_time 2 ./snapgram_thread 2048.txt /dev/null
	local tst_perf=${PERF}

	local ratio=$(bc -l <<<"${tst_perf} / ${ref_perf}")
	log "${ratio}"
}
TEST_CASES+=("thread_speed")

block_speed()
{
	run_time 2 ./ref_snapgram_block 2048.txt /dev/null
	local ref_perf=${PERF}

	run_time 2 ./snapgram_block 2048.txt /dev/null
	local tst_perf=${PERF}

	local ratio=$(bc -l <<<"${tst_perf} / ${ref_perf}")
	log "${ratio}"
}
TEST_CASES+=("block_speed")

#
# Main functions
#
parse_argvs() {
	local OPTIND opt

	while getopts "h?s:t:" opt; do
		case "$opt" in
			h|\?)
				echo "${SCRIPT_NAME}: [-t <test_case>]" 1>&2
				exit 0
				;;
			t)  TEST_CASE="${OPTARG}"
				;;
		esac
	done
}

check_vals() {
	# Check test case
	[[ " ${TEST_CASES[@]} all " =~ " ${TEST_CASE} " ]] || \
		die "Cannot find test case '${TEST_CASE}'"
}

grade() {
	# Run test case(s)
	if [[ "${TEST_CASE}" == "all" ]]; then
		# Run all test cases
		for t in "${TEST_CASES[@]}"; do
			log "--- Running test case: ${t} ---"
			${t}
			log "\n"
		done
	else
		# Run specific test case
		${TEST_CASE}
	fi
}

parse_argvs "$@"
check_vals
grade
