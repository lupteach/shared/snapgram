# Snapgram: matrix multiplications made more engaging

## Credits

This assignment is an original idea by Joël Porquet-Lupine at the LupLab and is
licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

## Files

- Assignment
    * `handout.html`: handout of assignment
- Scaffolding code for students
    * `snapgram.c`: main command-line interface
    * `sg_{serial,thread,block}.c`: implementations to be completed
- Datasets
    * `create_dataset.c`: source code of dataset generator
    * `create_dataset`: pre-compiled dataset generator for x86_64 GNU/Linux
    * `{8,512,1024,2048}.txt`: pre-generated datasets of various sizes
- References
    * `ref_%`: pre-compiled reference executables for x86_64 GNU/Linux
    * `{8,512,1024,2048}.ref`: reference output files
- Testing
    * `grade_snapgram.sh`: auto-grading script which tests for correctness
      (against reference output files) and speed (against reference executables)
- Compiling
    * `Makefile`: run `make` to compile code
